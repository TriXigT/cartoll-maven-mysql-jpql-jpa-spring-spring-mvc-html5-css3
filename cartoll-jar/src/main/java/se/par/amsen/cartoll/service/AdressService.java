package se.par.amsen.cartoll.service;

import java.util.List;

import se.par.amsen.cartoll.domain.Adress;

public interface AdressService {
	public long createAdress(Adress adress);
	public long updateAdress(Adress adress);
	public List<Adress> getAdresses();
	public Adress getAdressById(long id);
	public boolean removeAdress(long id);
	public void clearAllAdresses();
}
