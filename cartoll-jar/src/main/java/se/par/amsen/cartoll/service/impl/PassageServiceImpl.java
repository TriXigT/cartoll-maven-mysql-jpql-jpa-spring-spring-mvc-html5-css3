package se.par.amsen.cartoll.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.par.amsen.cartoll.domain.Passage;
import se.par.amsen.cartoll.domain.statistic.SimpleStatistic;
import se.par.amsen.cartoll.domain.statistic.TollStationStatistic;
import se.par.amsen.cartoll.repository.PassageRepository;
import se.par.amsen.cartoll.service.PassageService;
import se.par.amsen.cartoll.service.TaxIntervalService;

@Service
public class PassageServiceImpl implements PassageService{

	@Autowired
	private PassageRepository repository;
	
	@Autowired
	private TaxIntervalService taxService;
	
	@Override
	public long createPassageAndCalculateTax(Passage passage) {
		passage.setTax(taxService.calculateTax(passage));
		return repository.createPassage(passage);
	}

	@Override
	public List<Passage> getPassages() {
		return repository.getPassages();
	}

	@Override
	public Passage getPassageByPassageId(long id) {
		return repository.getPassageById(id);
	}

	@Override
	public long updatePassageAndCalculateTax(Passage passage) {
		passage.setTax(taxService.calculateTax(passage));
		return repository.updatePassage(passage);
	}

	@Override
	public boolean removePassage(long id) {
		return repository.removePassageById(id);
	}
	

	@Override
	public List<Passage> getPassagesForTollStation(long tollStationId) {
		return repository.getPassagesForTollStation(tollStationId);
	}
	
	@Override
	public TollStationStatistic getStatisticsByTollStationId(long id) {
		return repository.getStatisticsByTollStationId(id);
	}

	@Override
	public SimpleStatistic getStatisticsSummaryForAllTollStations() {
		return repository.getStatisticsSummaryForAllTollStations();
	}

	@Override
	public SimpleStatistic getStatisticsByVehicleId(long id) {
		return repository.getStatisticsByVehicleId(id);
	}

	@Override
	public void clearAllPassages() {
		repository.clearAllPassages();
	}
}
