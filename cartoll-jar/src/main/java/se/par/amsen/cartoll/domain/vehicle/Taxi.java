package se.par.amsen.cartoll.domain.vehicle;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import se.par.amsen.cartoll.domain.Owner;
import se.par.amsen.cartoll.domain.TaxLevel;

/**
 * Taxi overrides tax calculations with those specific for a Taxi and has an environment certificate
 * which is taken into the tax calculation
 * 
 * @author Pär
 *
 */
@Entity
@DiscriminatorValue(value="Taxi")
public class Taxi extends Vehicle {
	private boolean environmentCertificate;
	
	public Taxi(String regNumber, Owner owner, boolean environmentCertificate) {
		super(regNumber, owner);
		this.environmentCertificate = environmentCertificate;
	}
	
	public Taxi() { }

	public boolean hasEnvironmentCertificate() {
		return environmentCertificate;
	}

	@Override
	public int calculateTax(TaxLevel taxLevel) {
		int tax = getTaxByLevel(taxLevel);
		return hasEnvironmentCertificate() ? tax : tax + 5;
	}
	
	@Override
	public String getDetailedType() {
		return String.format("Taxi, %s", (environmentCertificate ? "has Environment Cert." : "no Environment Cert."));
	}
	
	@Override
	public String getDetailedName() {
		return String.format("Taxi | %s | (%s)", getRegNumber(), (environmentCertificate ? "Environment Cert." : "No Environment Cert."));
	}

	@Override
	public int getLowTax() {
		return 10;
	}

	@Override
	public int getMediumTax() {
		return 15;
	}

	@Override
	public int getHighTax() {
		return 20;
	}
}
