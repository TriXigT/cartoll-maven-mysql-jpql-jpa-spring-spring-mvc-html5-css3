package se.par.amsen.cartoll.repository;

import java.util.List;

import se.par.amsen.cartoll.domain.vehicle.Vehicle;

public interface VehicleRepository {
	public long createVehicle(Vehicle vehicle);
	public long updateVehicle(Vehicle vehicle);
	public List<Vehicle> getVehicles();
	public Vehicle getVehicleById(long id);
	public boolean removeVehicleById(long id);
	public void clearAllVehicles();
}
