package se.par.amsen.cartoll.repository.mock;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import se.par.amsen.cartoll.domain.Adress;
import se.par.amsen.cartoll.domain.Owner;
import se.par.amsen.cartoll.domain.Passage;
import se.par.amsen.cartoll.domain.TaxInterval;
import se.par.amsen.cartoll.domain.TaxLevel;
import se.par.amsen.cartoll.domain.TollStation;
import se.par.amsen.cartoll.domain.vehicle.Car;
import se.par.amsen.cartoll.domain.vehicle.Taxi;
import se.par.amsen.cartoll.domain.vehicle.Truck;
import se.par.amsen.cartoll.domain.vehicle.Vehicle;
import se.par.amsen.cartoll.service.AdressService;
import se.par.amsen.cartoll.service.OwnerService;
import se.par.amsen.cartoll.service.PassageService;
import se.par.amsen.cartoll.service.TaxIntervalService;
import se.par.amsen.cartoll.service.TollStationService;
import se.par.amsen.cartoll.service.VehicleService;

public class MockInitializer {

	Logger log = Logger.getLogger(MockInitializer.class);
	
	@Autowired
	private AdressService adressServ;
	
	@Autowired
	private OwnerService ownerServ;
	
	@Autowired
	private PassageService passageServ;
	
	@Autowired
	private TollStationService tollStationServ;
	
	@Autowired
	private VehicleService vehicleServ;
	
	@Autowired
	private TaxIntervalService taxIntervalServ;

	//So we can get instances for tests
	private Vehicle car1;
	private TollStation tollStation1;
	private TollStation tollStation2;
	
	public void init() {
		TaxInterval interval0 = taxIntervalServ.getTaxIntervalById(taxIntervalServ.createTaxInterval(new TaxInterval(0,0,05,59,TaxLevel.NO_TAX)));
		TaxInterval interval1 = taxIntervalServ.getTaxIntervalById(taxIntervalServ.createTaxInterval(new TaxInterval(6,0,6,29,TaxLevel.LOW)));
		TaxInterval interval2 = taxIntervalServ.getTaxIntervalById(taxIntervalServ.createTaxInterval(new TaxInterval(6,30,6,59,TaxLevel.MEDIUM)));
		TaxInterval interval3 = taxIntervalServ.getTaxIntervalById(taxIntervalServ.createTaxInterval(new TaxInterval(7,0,7,59,TaxLevel.HIGH)));
		TaxInterval interval4 = taxIntervalServ.getTaxIntervalById(taxIntervalServ.createTaxInterval(new TaxInterval(8,0,8,29,TaxLevel.MEDIUM)));
		TaxInterval interval5 = taxIntervalServ.getTaxIntervalById(taxIntervalServ.createTaxInterval(new TaxInterval(8,30,14,59,TaxLevel.LOW)));
		TaxInterval interval6 = taxIntervalServ.getTaxIntervalById(taxIntervalServ.createTaxInterval(new TaxInterval(15,0,15,29,TaxLevel.MEDIUM)));
		TaxInterval interval7 = taxIntervalServ.getTaxIntervalById(taxIntervalServ.createTaxInterval(new TaxInterval(15,30,16,59,TaxLevel.HIGH)));
		TaxInterval interval8 = taxIntervalServ.getTaxIntervalById(taxIntervalServ.createTaxInterval(new TaxInterval(17,0,17,59,TaxLevel.MEDIUM)));
		TaxInterval interval9 = taxIntervalServ.getTaxIntervalById(taxIntervalServ.createTaxInterval(new TaxInterval(18,0,18,29,TaxLevel.LOW)));
		TaxInterval interval10 = taxIntervalServ.getTaxIntervalById(taxIntervalServ.createTaxInterval(new TaxInterval(18,30,23,59,TaxLevel.NO_TAX)));
		
		Adress adress1 = adressServ.getAdressById(adressServ.createAdress(new Adress("Högerkroken", "111111", "Way Up")));
		Adress adress2 = adressServ.getAdressById(adressServ.createAdress(new Adress("Vänsterkroken", "777777", "Way Down")));
		
		Owner owner1 = ownerServ.getOwnerById(ownerServ.createOwner(new Owner("Linnea", "Amsen", "8512120000", adress1, new ArrayList<Vehicle>())));
		Owner owner2 = ownerServ.getOwnerById(ownerServ.createOwner(new Owner("Robert", "Tornberg", "8612120000", adress2, new ArrayList<Vehicle>())));
		
		car1 = vehicleServ.getVehicleById(vehicleServ.createVehicle(new Car("ABC-123", owner1)));
		Vehicle car2 = vehicleServ.getVehicleById(vehicleServ.createVehicle(new Car("ABC-123", owner1)));
		Vehicle car3= vehicleServ.getVehicleById(vehicleServ.createVehicle(new Car("ABC-123", owner2)));
		
		Vehicle truck1 = vehicleServ.getVehicleById(vehicleServ.createVehicle(new Truck("ABC-123", owner2, 5000)));
		Vehicle truck2 = vehicleServ.getVehicleById(vehicleServ.createVehicle(new Truck("ABC-123", owner1, 89000)));
		Vehicle truck3 = vehicleServ.getVehicleById(vehicleServ.createVehicle(new Truck("ABC-123", owner2, 14799)));
		
		Vehicle taxi1 = vehicleServ.getVehicleById(vehicleServ.createVehicle(new Taxi("ABC-123", owner2, true)));
		Vehicle taxi2 = vehicleServ.getVehicleById(vehicleServ.createVehicle(new Taxi("ABC-123", owner1, false)));
		Vehicle taxi3 = vehicleServ.getVehicleById(vehicleServ.createVehicle(new Taxi("ABC-123", owner1, true)));
		
		owner1.getVehicles().add(car1);
		owner1.getVehicles().add(car2);
		owner1.getVehicles().add(truck2);
		owner1.getVehicles().add(taxi2);
		owner1.getVehicles().add(taxi3);

		owner2.getVehicles().add(car3);
		owner2.getVehicles().add(truck1);
		owner2.getVehicles().add(truck3);
		owner2.getVehicles().add(taxi1);
		
		tollStation1 = tollStationServ.getTollStationById(tollStationServ.createTollStation(new TollStation("Himmelen", adress1)));
		tollStation2 = tollStationServ.getTollStationById(tollStationServ.createTollStation(new TollStation("Helvetet", adress2)));
		
		passageServ.getPassageByPassageId(passageServ.createPassageAndCalculateTax(new Passage(car1, 12, 18, tollStation1)));
		passageServ.getPassageByPassageId(passageServ.createPassageAndCalculateTax(new Passage(car2, 11, 23, tollStation2)));
		passageServ.getPassageByPassageId(passageServ.createPassageAndCalculateTax(new Passage(car3, 3, 0, tollStation1)));
		passageServ.getPassageByPassageId(passageServ.createPassageAndCalculateTax(new Passage(car1, 0, 1, tollStation2)));
		passageServ.getPassageByPassageId(passageServ.createPassageAndCalculateTax(new Passage(car2, 1, 55, tollStation1)));
		passageServ.getPassageByPassageId(passageServ.createPassageAndCalculateTax(new Passage(truck1, 2, 55, tollStation2)));
		passageServ.getPassageByPassageId(passageServ.createPassageAndCalculateTax(new Passage(truck1, 8, 47, tollStation1)));
		passageServ.getPassageByPassageId(passageServ.createPassageAndCalculateTax(new Passage(truck2, 5, 25, tollStation2)));
		passageServ.getPassageByPassageId(passageServ.createPassageAndCalculateTax(new Passage(truck2, 9, 22, tollStation1)));
		passageServ.getPassageByPassageId(passageServ.createPassageAndCalculateTax(new Passage(truck3, 10, 55, tollStation2)));
		passageServ.getPassageByPassageId(passageServ.createPassageAndCalculateTax(new Passage(truck3, 3, 5, tollStation1)));
		passageServ.getPassageByPassageId(passageServ.createPassageAndCalculateTax(new Passage(taxi1, 3, 1, tollStation2)));
		passageServ.getPassageByPassageId(passageServ.createPassageAndCalculateTax(new Passage(taxi1, 15, 0, tollStation1)));
		passageServ.getPassageByPassageId(passageServ.createPassageAndCalculateTax(new Passage(taxi2, 17, 15, tollStation2)));
		passageServ.getPassageByPassageId(passageServ.createPassageAndCalculateTax(new Passage(taxi3, 1, 32, tollStation1)));
	}

	public Vehicle getCar1() {
		return car1;
	}

	public void setCar1(Vehicle car1) {
		this.car1 = car1;
	}

	public TollStation getTollStation1() {
		return tollStation1;
	}

	public void setTollStation1(TollStation tollStation1) {
		this.tollStation1 = tollStation1;
	}

	public TollStation getTollStation2() {
		return tollStation2;
	}

	public void setTollStation2(TollStation tollStation2) {
		this.tollStation2 = tollStation2;
	}
	
}
