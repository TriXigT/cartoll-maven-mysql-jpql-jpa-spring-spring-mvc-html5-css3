package se.par.amsen.cartoll.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * An adress holds data about which street, city and postal code a physical entity has
 * @author Pär
 *
 */
@Entity
@NamedQueries(
		value={	@NamedQuery(name="getAllAdresses", query="SELECT a FROM Adress a ORDER BY street,city,postalCode ASC"),
				@NamedQuery(name="clearAllAdresses", query="DELETE FROM Adress") })
public class Adress {
	
	@Id
	@GeneratedValue
	private long id;
	private String street;
	private String city;
	private String postalCode;
	
	public Adress(String street, String city, String postalCode) {
		this.street = street;
		this.city = city;
		this.postalCode = postalCode;
	}
	
	public Adress() { }

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}	
}
