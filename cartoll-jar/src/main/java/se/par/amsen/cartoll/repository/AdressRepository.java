package se.par.amsen.cartoll.repository;

import java.util.List;

import se.par.amsen.cartoll.domain.Adress;

public interface AdressRepository {
	public long createAdress(Adress adress);
	public long updateAdress(Adress adress);
	public List<Adress> getAdresses();
	public Adress getAdressById(long id);
	public boolean removeAdressById(long id);
	public void clearAllAdresses();
}
