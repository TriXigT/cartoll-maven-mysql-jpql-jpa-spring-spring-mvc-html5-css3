package se.par.amsen.cartoll.domain.vehicle;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import se.par.amsen.cartoll.domain.Owner;
import se.par.amsen.cartoll.domain.TaxLevel;

/**
 * Vehicle is the abstract super class of all Vehicle children,
 * a Vehicle has a registration number and four different tax levels which
 * implementing subclasses must override with type specific values.
 * @author Pär
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="vehicleType", discriminatorType=DiscriminatorType.STRING, length=10)
@NamedQueries(value={	@NamedQuery(name="getAllVehicles", query="SELECT v FROM Vehicle v ORDER BY owner.lastName, owner.firstName, regNumber ASC"),
						@NamedQuery(name="clearAllVehicles", query="DELETE FROM Vehicle")})
public abstract class Vehicle {
	
	@Id
	@GeneratedValue
	private long id;
	
	@Column(length=7)
	private String regNumber;
	
	@ManyToOne(cascade=CascadeType.MERGE)
	@JoinColumn(name="ownerId")
	private Owner owner;

	public Vehicle(String regNumber, Owner owner) {
		this.regNumber = regNumber;
		this.owner = owner;
	}
	
	public Vehicle(){ }
	
	/**
	 * Calculate tax for a TaxLevel Enum constant.
	 * @param int taxLevel 
	 * @return Tax amount calculated specifically for the vehicle type
	 */
	public abstract int calculateTax(TaxLevel taxLevel);
	
	/**
	 * Get tax amount for a TaxLevel Enum constant
	 * @param TaxLevel tax level Enum constant
	 * @return Tax amount in SEK or -1 if invalid TaxLevel
	 */
	public int getTaxByLevel(TaxLevel taxLevel) {
		switch(taxLevel) {
		case NO_TAX:
			return getNoTax();
		case LOW:
			return getLowTax();
		case MEDIUM:
			return getMediumTax();
		case HIGH:
			return getHighTax();
		default:
			return -1;
		}
	}
	
	/**
	 * Format a String representing the detailed type for a Vehicle, 
	 * ex. "Truck | Weight: 5564kg".
	 * Will not specify a name/reg, just the details and type for a Vehicle.
	 */
	public abstract String getDetailedType();
	
	/**
	 * Format a String representing the detailed name for a Vehicle, 
	 * ex. "Truck | ABC-123 | Weight: 5564kg".
	 * Specifies details and id-related info of a Vehicle, ex. the registration number.
	 */
	public abstract String getDetailedName();

	public String getRegNumber() {
		return regNumber;
	}

	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getNoTax() {
		return 0;
	}
	
	public abstract int getLowTax();
	public abstract int getMediumTax();
	public abstract int getHighTax();
}
