package se.par.amsen.cartoll.repository.jpa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class AbstractRepositoryJpa {
	@PersistenceContext
	EntityManager em;
}
