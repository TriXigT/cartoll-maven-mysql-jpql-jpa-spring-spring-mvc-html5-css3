package se.par.amsen.cartoll.repository;

import java.util.List;

import se.par.amsen.cartoll.domain.TollStation;

public interface TollStationRepository {
	public long createTollStation(TollStation tollStation);
	public long updateTollStation(TollStation tollStation);
	public List<TollStation> getTollStations();
	public TollStation getTollStationById(long id);
	public boolean removeTollStationById(long id);
	public void clearAllTollStations();
}
