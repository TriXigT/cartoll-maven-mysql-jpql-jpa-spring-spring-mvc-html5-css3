package se.par.amsen.cartoll.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.par.amsen.cartoll.domain.Owner;
import se.par.amsen.cartoll.repository.OwnerRepository;
import se.par.amsen.cartoll.service.OwnerService;

@Service
public class OwnerServiceImpl implements OwnerService{

	@Autowired
	private OwnerRepository repository;
	
	@Override
	public List<Owner> getOwners() {
		return repository.getOwners();
	}

	@Override
	public Owner getOwnerById(long id) {
		return repository.getOwnerById(id);
	}

	@Override
	public long createOwner(Owner owner) {
		return repository.createOwner(owner);
	}

	@Override
	public long updateOwner(Owner owner) {
		return repository.updateOwner(owner);
	}

	@Override
	public boolean removeOwner(long id) {
		return repository.removeOwnerById(id);
	}

	@Override
	public void clearAllOwners() {
		repository.clearAllOwners();
	}
}
