package se.par.amsen.cartoll.repository.jpa;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.QueryException;
import org.springframework.transaction.annotation.Transactional;

import se.par.amsen.cartoll.domain.Passage;
import se.par.amsen.cartoll.domain.statistic.SimpleStatistic;
import se.par.amsen.cartoll.domain.statistic.TollStationStatistic;
import se.par.amsen.cartoll.repository.PassageRepository;

public class PassageRepositoryJpa extends AbstractRepositoryJpa implements PassageRepository {

	
	@Transactional
	@Override
	public long createPassage(Passage passage) {
		em.persist(passage);
		return passage.getId();
	}

	@Transactional
	@Override
	public long updatePassage(Passage passage) {
		return em.merge(passage).getId();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Passage> getPassages() {
		return em.createNamedQuery("getAllPassages").getResultList();
	}

	@Override
	public Passage getPassageById(long id) {
		return em.find(Passage.class, id);
	}

	@Transactional
	@Override
	public boolean removePassageById(long id) {
		Passage passage = em.find(Passage.class, id);
		em.remove(passage);
		return em.find(Passage.class, id) == null;
	}

	@Transactional
	@Override
	public void clearAllPassages() {
		em.createNamedQuery("clearAllPassages").executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Passage> getPassagesForTollStation(long id) {
		Query query = em.createNamedQuery("getPassagesForTollStation");
		query.setParameter(1, id);
		return query.getResultList();
	}

	@Override
	public TollStationStatistic getStatisticsByTollStationId(long id) {
		Query query = em.createNamedQuery("getStatisticsByTollStationId");
		query.setParameter(1, id);
		
		TollStationStatistic stat;
		try{
			stat = (TollStationStatistic) query.getSingleResult();
		} catch(IllegalArgumentException e) {
			//No passages found etc.
			return new TollStationStatistic(id, 0, 0);
		}
		
		return stat;
	}

	@Override
	public SimpleStatistic getStatisticsSummaryForAllTollStations() {
		Query query = em.createNamedQuery("getStatisticsSummaryForAllTollStations");
		
		SimpleStatistic stat;
		try{
			stat = (SimpleStatistic) query.getSingleResult();
		} catch(IllegalArgumentException e) {
			//No passages found etc.
			return new SimpleStatistic(0, 0);
		}
		
		return stat;
	}

	@Override
	public SimpleStatistic getStatisticsByVehicleId(long id) {
		Query query = em.createNamedQuery("getStatisticsByVehicleId");
		query.setParameter(1, id);

		SimpleStatistic stat;
		
		try{
			stat = (SimpleStatistic) query.getSingleResult();
		} catch(IllegalArgumentException e) {
			//No passages found etc.
			return new SimpleStatistic(0, 0);
		}
		
		return stat;
	}

}
