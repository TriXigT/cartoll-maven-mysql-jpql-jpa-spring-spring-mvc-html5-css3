package se.par.amsen.cartoll.repository.mock;

import java.util.ArrayList;
import java.util.List;

import se.par.amsen.cartoll.domain.TaxInterval;
import se.par.amsen.cartoll.domain.TaxLevel;
import se.par.amsen.cartoll.repository.TaxIntervalRepository;

public class TaxIntervalRepositoryMock extends GenerateUniqueIdMock implements TaxIntervalRepository {

	private TaxInterval interval0 = new TaxInterval(0,0,05,59,TaxLevel.NO_TAX);
	private TaxInterval interval1 = new TaxInterval(6,0,6,29,TaxLevel.LOW);
	private TaxInterval interval2 = new TaxInterval(6,30,6,59,TaxLevel.MEDIUM);
	private TaxInterval interval3 = new TaxInterval(7,0,7,59,TaxLevel.HIGH);
	private TaxInterval interval4 = new TaxInterval(8,0,8,29,TaxLevel.MEDIUM);
	private TaxInterval interval5 = new TaxInterval(8,30,14,59,TaxLevel.LOW);
	private TaxInterval interval6 = new TaxInterval(15,0,15,29,TaxLevel.MEDIUM);
	private TaxInterval interval7 = new TaxInterval(15,30,16,59,TaxLevel.HIGH);
	private TaxInterval interval8 = new TaxInterval(17,0,17,59,TaxLevel.MEDIUM);
	private TaxInterval interval9 = new TaxInterval(18,0,18,29,TaxLevel.LOW);
	private TaxInterval interval10 = new TaxInterval(18,30,23,59,TaxLevel.NO_TAX);

	public final List<TaxInterval> taxIntervals;

	public TaxIntervalRepositoryMock(){
		taxIntervals = new ArrayList<TaxInterval>();
		taxIntervals.add(interval0);
		taxIntervals.add(interval1);
		taxIntervals.add(interval2);
		taxIntervals.add(interval3);
		taxIntervals.add(interval4);
		taxIntervals.add(interval5);
		taxIntervals.add(interval6);
		taxIntervals.add(interval7);
		taxIntervals.add(interval8);
		taxIntervals.add(interval9);
		taxIntervals.add(interval10);
	}

	@Override
	public List<TaxInterval> getTaxIntervals() {
		return taxIntervals;
	}

	@Override
	public long createTaxInterval(TaxInterval taxInterval) {
		taxIntervals.add(taxInterval);
		taxInterval.setId(generateUniqueId());
		return taxInterval.getId();
	}

	@Override
	public long updateTaxInterval(TaxInterval taxInterval) {
		taxIntervals.add(taxInterval);
		return taxInterval.getId();
	}

	@Override
	public TaxInterval getTaxIntervalById(long id) {
		for(TaxInterval taxInterval : taxIntervals) {
			if(taxInterval.getId() == id) 
				return taxInterval;
		}
		return null;
	}

	@Override
	public boolean removeTaxIntervalById(long id) {
		for(TaxInterval taxInterval : taxIntervals) {
			if(taxInterval.getId() == id) {
				taxIntervals.remove(taxInterval);
				return true;
			}
		}
		return false;
	}

	@Override
	public void clearAllTaxIntervals() {
		taxIntervals.clear();
	}

}
