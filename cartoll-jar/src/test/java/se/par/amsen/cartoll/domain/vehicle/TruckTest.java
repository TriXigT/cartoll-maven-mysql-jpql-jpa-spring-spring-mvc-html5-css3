package se.par.amsen.cartoll.domain.vehicle;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import se.par.amsen.cartoll.domain.Owner;
import se.par.amsen.cartoll.domain.TaxLevel;
import se.par.amsen.cartoll.domain.vehicle.Truck;

public class TruckTest {
	private final static String REG_NUMBER = "ABC-123";
	private final static int WEIGHT = 14000;
	private final static Owner OWNER = new Owner(null,null,null,null,null);
	
	private final static TaxLevel TAX_LEVEL = TaxLevel.MEDIUM;

	private Truck truck; 

	@Before
	public void setup() {
		truck = new Truck(REG_NUMBER, OWNER, WEIGHT);
	}

	@Test
	public void testConstructor() {
		assertEquals("Registration Number", REG_NUMBER, truck.getRegNumber());
		assertEquals("Registration Number", WEIGHT, truck.getWeightInKg());
		assertEquals("Owner", OWNER, truck.getOwner());
	}
	
	@Test
	public void testTaxCalculation() {
		//Calculate tax amount + weight fee
		int weightPlusTax = (int) (truck.getTaxByLevel(TAX_LEVEL) + (Math.floor(((WEIGHT - Truck.FREE_WEIGHT_IN_KG)/1000))) * Truck.TAX_PER_TON);
		
		//Calculate tax amount
		assertEquals("Calculate tax amount for tax level: " + TAX_LEVEL, weightPlusTax, truck.calculateTax(TAX_LEVEL));
	}
}
