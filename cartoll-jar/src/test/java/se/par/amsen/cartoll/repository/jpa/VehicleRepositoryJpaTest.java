package se.par.amsen.cartoll.repository.jpa;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import javax.persistence.PersistenceException;

import org.hibernate.exception.DataException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import se.par.amsen.cartoll.domain.Adress;
import se.par.amsen.cartoll.domain.Owner;
import se.par.amsen.cartoll.domain.vehicle.Car;
import se.par.amsen.cartoll.domain.vehicle.Vehicle;
import se.par.amsen.cartoll.repository.AdressRepository;
import se.par.amsen.cartoll.repository.OwnerRepository;
import se.par.amsen.cartoll.repository.PassageRepository;
import se.par.amsen.cartoll.repository.VehicleRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/application_context_mock_jpa.xml"})
public class VehicleRepositoryJpaTest {
	@Autowired
	VehicleRepository repo;
	
	@Autowired
	OwnerRepository ownerRepo;
	
	@Autowired
	AdressRepository adressRepo;
	
	@Autowired
	PassageRepository passageRepo;
	
	Vehicle car;
	Owner owner;
	Adress adress;
	
	@Before
	public void setUp() throws Exception {
		passageRepo.clearAllPassages();
		repo.clearAllVehicles();
		ownerRepo.clearAllOwners();
		adressRepo.clearAllAdresses();
		
		adress = adressRepo.getAdressById(adressRepo.createAdress(new Adress(" ", " ", " ")));
		owner = ownerRepo.getOwnerById(ownerRepo.createOwner(new Owner(" ", " ", " ", adress, new ArrayList<Vehicle>())));
		car = repo.getVehicleById(repo.createVehicle(new Car("ABC-123", owner)));
		
		owner.getVehicles().add(car);
	}
	
	@After
	public void tearDown() {
		repo.clearAllVehicles();
		ownerRepo.clearAllOwners();
		adressRepo.clearAllAdresses();
	}


	@Test(expected=PersistenceException.class)
	public void testCreateVehicle() {
		assertEquals("Test Vehicle id", true, repo.createVehicle(new Car("AVS-123", owner)) > 0);
		
		//Should gen error due to max RegNo length being 7
		repo.createVehicle(new Car("FAULTY REG NO", owner));
	}

	@Test
	public void testUpdateVehicle() {
		car.setRegNumber("111-111");
		repo.updateVehicle(car);
		
		assertEquals("Vehicle is updated", "111-111", repo.getVehicleById(car.getId()).getRegNumber());
	}

	@Test
	public void testGetVehicles() {
		//car created to repo in setup
		repo.createVehicle(new Car("GDE-123", owner));
		repo.createVehicle(new Car("DEF-123", owner));
		
		assertEquals("No. of Vehicles are 3", 3, repo.getVehicles().size());
	}

	@Test
	public void testGetVehicleById() {
		assertEquals("Get Vehicle", car.getId(), repo.getVehicleById(car.getId()).getId());
	}

	@Test
	public void testRemoveVehicleById() {
		assertEquals("Vehicle created", true, repo.getVehicleById(car.getId()) != null);
		assertEquals("Vehicle removed", true, repo.removeVehicleById(car.getId()));
	}
}
