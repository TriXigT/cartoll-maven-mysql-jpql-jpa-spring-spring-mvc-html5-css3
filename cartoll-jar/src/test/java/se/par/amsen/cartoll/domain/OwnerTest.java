package se.par.amsen.cartoll.domain;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import se.par.amsen.cartoll.domain.vehicle.Vehicle;

public class OwnerTest {

	private final static String FIRST_NAME = "Pär-Nils";
	private final static String LAST_NAME = "Amsen";
	private final static String SSID = "Göteborg";
	private final static Adress ADRESS = new Adress(null,null,null);
	private final static List<Vehicle> VEHICLES = new ArrayList<Vehicle>();

	private Owner owner; 
	
	@Before
	public void setup() {
		owner = new Owner(FIRST_NAME, LAST_NAME, SSID, ADRESS,VEHICLES);
	}
	
	@Test
	public void testConstructor() {
		assertEquals("First name", FIRST_NAME, owner.getFirstName());
		assertEquals("Last name", LAST_NAME, owner.getLastName());
		assertEquals("SSID", SSID, owner.getSSID());
		assertEquals("Adress", ADRESS, owner.getAdress());
		assertEquals("Vehicles", VEHICLES, owner.getVehicles());
	}

}
