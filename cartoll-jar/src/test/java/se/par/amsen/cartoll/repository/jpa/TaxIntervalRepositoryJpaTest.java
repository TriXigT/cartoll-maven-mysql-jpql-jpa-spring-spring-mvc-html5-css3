package se.par.amsen.cartoll.repository.jpa;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import se.par.amsen.cartoll.domain.Owner;
import se.par.amsen.cartoll.domain.TaxInterval;
import se.par.amsen.cartoll.domain.TaxLevel;
import se.par.amsen.cartoll.repository.TaxIntervalRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/application_context_mock_jpa.xml"})
public class TaxIntervalRepositoryJpaTest {
	@Autowired
	TaxIntervalRepository repo;
	
	TaxInterval taxInterval = new TaxInterval(3,45,12,0,TaxLevel.HIGH);

	@Before
	public void setUp() throws Exception {
		repo.clearAllTaxIntervals();
	}
	
	@After
	public void tearDown() {
		repo.clearAllTaxIntervals();
	}

	@Test
	public void testCreateTaxInterval() {
		assertEquals("Test TaxInterval id", true, repo.createTaxInterval(taxInterval) > 0);
	}

	@Test
	public void testUpdateTaxInterval() {
		repo.createTaxInterval(taxInterval);
		taxInterval.setTaxLevel(TaxLevel.NO_TAX);
		repo.updateTaxInterval(taxInterval);
		
		assertEquals("TaxInterval is updated", TaxLevel.NO_TAX, repo.getTaxIntervalById(taxInterval.getId()).getTaxLevel());
	}

	@Test
	public void testGetTaxIntervals() {
		repo.createTaxInterval(new TaxInterval(3,45,12,0,TaxLevel.HIGH));
		repo.createTaxInterval(new TaxInterval(5,45,12,0,TaxLevel.LOW));
		repo.createTaxInterval(new TaxInterval(7,45,12,0,TaxLevel.NO_TAX));
		
		assertEquals("No. of TaxIntervals are 3", 3, repo.getTaxIntervals().size());
	}

	@Test
	public void testGetTaxIntervalById() {
		repo.createTaxInterval(taxInterval);
		assertEquals("Get TaxInterval", taxInterval.getId(), repo.getTaxIntervalById(taxInterval.getId()).getId());
	}

	@Test
	public void testRemoveTaxIntervalById() {
		repo.createTaxInterval(taxInterval);
		repo.removeTaxIntervalById(taxInterval.getId());
		assertEquals("TaxInterval removed", 0, repo.getTaxIntervals().size());
	}
}
