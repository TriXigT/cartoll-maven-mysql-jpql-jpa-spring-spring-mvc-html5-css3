<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value="/resources/styles/reset.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/styles/main.css" />"
	rel="stylesheet">
<title>View TollStation</title>
</head>
<body>
	<div class="container">
		<jsp:include page="header.jsp"></jsp:include>
		<div class="content">
			<div id="stations">
				<div id="stationsList">
				<h1>Station</h1>
					<c:choose>
						<c:when test="${empty passages}">
							<p>Unable to load passages or this station has none</p>
						</c:when>
						<c:otherwise>
							<table class="stationTable">
								<tr>
									<th>Reg no.</th>
									<th>Vehicle type</th>
									<th>Owner</th>
									<th>Tax</th>
									<th>Date</th>
								</tr>
								<c:forEach items="${passages}" var="passage">
									<tr>
										<td>${passage.vehicle.regNumber}</td>
										<td>${passage.vehicle.detailedType}</td>
										<td><a href="<c:url value="/viewOwner/${passage.vehicle.owner.id}.html"/>">${passage.vehicle.owner.fullName}</a></td>
										<td>${passage.tax}</td>
										<td>${passage.formattedTime}</td>
									</tr>
								</c:forEach>
							</table>
						</c:otherwise>
					</c:choose>
				</div>
				<div id="stationsStatistics">
					<h1>Adress</h1>
					<p>Street:</p><span>${station.adress.street}</span>
					<p>Postal code:</p><span>${station.adress.postalCode}</span>
					<p>City:</p><span>${station.adress.city}</span>
				</div>
			</div>
		</div>
	</div>
</body>
</html>