package se.par.amsen.cartoll.mvc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.par.amsen.cartoll.domain.Owner;
import se.par.amsen.cartoll.domain.statistic.SimpleStatistic;
import se.par.amsen.cartoll.domain.vehicle.Vehicle;
import se.par.amsen.cartoll.service.OwnerService;
import se.par.amsen.cartoll.service.PassageService;
import se.par.amsen.cartoll.service.VehicleService;

@Controller
public class ViewOwnerController {
	
	@Autowired
	OwnerService ownerService;

	@Autowired
	VehicleService vehicleService;
	
	@Autowired
	PassageService passageService;
	
	@RequestMapping("/viewOwner/{ownerId}")
	public ModelAndView get(@PathVariable long ownerId) {
		ModelAndView mav = new ModelAndView("viewOwner");
		
		Owner owner = ownerService.getOwnerById(ownerId);
		
		List<VehicleBean> vehicleBeans = new ArrayList<VehicleBean>();
		
		for(Vehicle vehicle : owner.getVehicles()) {
			VehicleBean vehicleBean = new VehicleBean();
			vehicleBean.setVehicle(vehicle);
			vehicleBean.setStatistic(passageService.getStatisticsByVehicleId(vehicle.getId()));
			vehicleBeans.add(vehicleBean);
		}
		
		mav.addObject("vehicleBeans", vehicleBeans);
		mav.addObject("owner", owner);
		return mav;
	}
	
	public static class VehicleBean {

		Vehicle vehicle;
		SimpleStatistic statistic;
		
		public Vehicle getVehicle() {
			return vehicle;
		}
		public void setVehicle(Vehicle vehicle) {
			this.vehicle = vehicle;
		}
		public SimpleStatistic getStatistic() {
			return statistic;
		}
		public void setStatistic(SimpleStatistic statistic) {
			this.statistic = statistic;
		}
	}
}
