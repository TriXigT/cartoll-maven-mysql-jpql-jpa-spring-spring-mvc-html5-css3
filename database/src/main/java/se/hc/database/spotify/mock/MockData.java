package se.hc.database.spotify.mock;

import java.util.HashSet;
import java.util.Set;

import se.hc.database.spotify.Album;
import se.hc.database.spotify.Artist;

/**
 * Some mock data used in the Spotify test.
 * 
 */
public class MockData {

	public static final long ARTIST_ABBA = 1;
	public static final long ARTIST_BEATLES = 2;
	public static final long ARTIST_MANDODIAO = 3;

	public static Set<Artist> getMockArtists() {

		HashSet<Artist> artists = new HashSet<Artist>();

		Artist abba = new Artist(ARTIST_ABBA, "Abba");
		Artist beatles = new Artist(ARTIST_BEATLES, "Beatles");
		Artist mondoDiao = new Artist(ARTIST_MANDODIAO, "Mando Diao");

		artists.add(abba);
		artists.add(beatles);
		artists.add(mondoDiao);

		Album album1 = new Album(1, abba, "Waterloo", 1974);
		abba.getAlbums().add(album1);

		Album album2 = new Album(2, mondoDiao, "Infruset", 2012);
		mondoDiao.getAlbums().add(album2);

		return artists;
	}

}
