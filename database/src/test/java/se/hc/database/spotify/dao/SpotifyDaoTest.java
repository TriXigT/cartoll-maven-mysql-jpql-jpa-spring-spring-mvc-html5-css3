package se.hc.database.spotify.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import se.hc.database.spotify.Album;
import se.hc.database.spotify.Artist;
import se.hc.database.spotify.mock.MockData;

public class SpotifyDaoTest {

	SpotifyDao dao;

	@Before
	public void setup() throws Exception {

		dao = new SpotifyDao();
		dao.init(MockData.getMockArtists());

	}

	/**
	 * Find an artist by ID
	 */
	@Test
	public void testGetArtistById() throws Exception {

		Artist abba = dao.getArtist(MockData.ARTIST_ABBA);
		assertNotNull("Artist is not null", abba);
		assertEquals("Name is correct", "Abba", abba.getName());

		Artist beatles = dao.getArtist(MockData.ARTIST_BEATLES);
		assertNotNull("Beatles is not null", beatles);
		assertEquals("Name is correct", "Beatles", beatles.getName());
	}

	/**
	 * Delete artist
	 */
	@Test
	public void testDeleteArtists() throws Exception {

		Artist artist = dao.getArtist(MockData.ARTIST_ABBA);
		assertNotNull("Artist is not null", artist);

		dao.deleteArtist(MockData.ARTIST_ABBA);

		artist = dao.getArtist(MockData.ARTIST_ABBA);
		assertNull("Artist should be null", artist);

	}

	/**
	 * Get the total number of artist
	 */
	@Test
	public void testGetArtistSize() throws Exception {

		int artistsSize = MockData.getMockArtists().size();
		assertEquals("Size before delete", artistsSize, dao.getArtistsSize());

		dao.deleteArtist(MockData.ARTIST_MANDODIAO);
		assertEquals("Size after delete", artistsSize - 1, dao.getArtistsSize());
	}

	/**
	 * Create a new Artist. No ID is given, that should be assigned automatically by the DAO.
	 */
	@Test
	public void testAddArtist() throws Exception {

		int artistsSize = MockData.getMockArtists().size();
		Artist artist = new Artist("New artist");
		long id = dao.createArtist(artist);
		assertEquals("New ID", artistsSize + 1, id);

		Artist persistedArtist = dao.getArtist(id);
		assertEquals("Name", artist.getName(), persistedArtist.getName());
	}

	/**
	 * Updates an existing artist
	 */
	@Test
	public void testUpdateArtist() throws Exception {
		Artist artist = new Artist("New artist");
		long id = dao.createArtist(artist);

		artist = dao.getArtist(id);
		artist.setName("Updated name");
		dao.updateArtist(artist);

		Artist updatedArtist = dao.getArtist(id);
		assertEquals("Name after update", "Updated name", updatedArtist.getName());
	}

	/**
	 * Retrieve an artist and related albums. This means we have to start storing albums when we store the artist
	 */
	@Test
	public void testGetArtistWithAlbums() throws Exception {

		Artist beatles = dao.getArtist(MockData.ARTIST_BEATLES);
		assertEquals("No of albums for beatles", 0, beatles.getAlbums().size());

		Artist mandoDiao = dao.getArtist(MockData.ARTIST_MANDODIAO);
		assertEquals("No of albums for Mando Diao", 1, mandoDiao.getAlbums().size());
	}

	/**
	 * Tests that the updateArtistNameAndAlbumName() method works as expected when there are no problems. In later tests
	 * we will add more transaction capability
	 * 
	 * Note that updateArtistNameAndAlbumName() is a strange method, used only two do two different updates in one
	 * method call to test transactions.
	 */
	@Test
	public void testTransactions_everything_is_ok() throws Exception {

		Artist mandoDiao = dao.getArtist(MockData.ARTIST_MANDODIAO);
		Album album = mandoDiao.getAlbums().iterator().next();
		dao.updateArtistNameAndAlbumName(mandoDiao, "Mando", album, "Frusen");

		// Verify that name has changed
		Artist newArtist = dao.getArtist(MockData.ARTIST_MANDODIAO);
		assertEquals("Artist name is changed", "Mando", newArtist.getName());
		Album newAlbum = newArtist.getAlbums().iterator().next();
		assertEquals("Album name is changed", "Frusen", newAlbum.getName());
	}

	/**
	 * Tests that an exception is thrown when we try to change to an artist name that already exists. The database table
	 * has to be updated so that the name column can only contain unique values. The whole operation should be wrapped
	 * in a transaction so that if either artist name or album name fails, none of the operations succeed.
	 */
	@Test
	public void testTransactions_artist_name_not_unique() throws Exception {

		Artist mandoDiao = dao.getArtist(MockData.ARTIST_MANDODIAO);
		Album album = mandoDiao.getAlbums().iterator().next();

		try {
			dao.updateArtistNameAndAlbumName(mandoDiao, "Abba", album, "Frusen");
			fail("Expected an exception when calling dao but got none");
		} catch (Exception e) {
			// We expected an exception, do nothing here
		}

		// Verify that nothing was updated
		Artist newArtist = dao.getArtist(MockData.ARTIST_MANDODIAO);
		assertEquals("Artist name has not changed", "Mando Diao", newArtist.getName());
		Album newAlbum = newArtist.getAlbums().iterator().next();
		assertEquals("Album name has not changed", "Infruset", newAlbum.getName());
	}

	/**
	 * Tests that an exception is thrown when we try to change to an album name that already exists.
	 */
	@Test
	public void testTransactions_album_name_not_unique() throws Exception {

		Artist mandoDiao = dao.getArtist(MockData.ARTIST_MANDODIAO);
		Album album = mandoDiao.getAlbums().iterator().next();

		try {
			dao.updateArtistNameAndAlbumName(mandoDiao, "Mando Diao 2", album, "Waterloo");
			fail("Expected an exception when calling dao but got none");
		} catch (Exception e) {
			// We expected an exception, do nothing here
		}

		// Verify that nothing was updated
		Artist newArtist = dao.getArtist(MockData.ARTIST_MANDODIAO);
		assertEquals("Artist name has not changed", "Mando Diao", newArtist.getName());
		Album newAlbum = newArtist.getAlbums().iterator().next();
		assertEquals("Album name has not changed", "Infruset", newAlbum.getName());
	}

}
